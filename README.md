# DEVOPS PROJECT

This projects implement Continous Integration and deploy in a virtual box.

Things you may want to cover:

* Ruby version
```
Ruby 2.5.0
```

* Database initialization
```
rails db:migrate
```
* Run test
```
rails db:migrate RAILS_ENV='TEST'
```
```
rspec
```
* Deployment instructions
```
rails Server
```
